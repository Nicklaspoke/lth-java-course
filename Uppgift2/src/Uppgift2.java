/**
 * The function of this class is to take two inputs of time for then a worker starts and ends his/her shift and then calculate his/her salary from a constant hourly rate
 * @author Nicklas König, nickni-6
 *
 */
//Importing the scanner that is needed for the input//
import java.util.Scanner;

public class Uppgift2 
{
	public static void main(String[] args)
	{
		//Declaring the scanner that will be used for the time inputs//
		Scanner input = new Scanner(System.in);
		
		input.useDelimiter("[:\\s]+");
		
		//Declaring variables for hour, minute and result for the salary//
		float starting_hour;
		float starting_min;
		float end_hour;
		float end_min;
		float salary;
		
		//Text output and input for the worker to type when he began working that day//
		System.out.print("Skriv in när du började jobba idag [hh:mm]>");
		starting_hour = input.nextFloat();
		starting_min = input.nextFloat();
		
		//Text output and input for the worker to type when he stopped working that day//
		System.out.print("Skriv in när du slutade jobba idag [hh:mm]>");
		end_hour = input.nextFloat();
		end_min = input.nextFloat();
		
		//Calculating the salary//
		float start_time = (starting_hour + (starting_min / 60));
		float end_time = (end_hour + (end_min / 60));
		salary = (end_time - start_time) * 190;
		
		//Presenting the salary//
		System.out.print("Din lön för idag blir: " + String.format("%.2f", salary));
		
		//Closing the Scanner//
		input.close();
		
	}
	
}
