/**
 * This program will take the users input of a number and print a triangle with letters, the user can choose if it shall be with the straight 
 * angle up or down, as well as the length of it.
 * @author Nicklas König, nickni-6
 *
 */

//Importing the scanner//
import java.util.Scanner;

import org.w3c.dom.css.Counter;

public class Uppgift3 
{
	public static void main(String[] args)
	{
		int length = 0;		//The length of the triangle//
		int upOrDown = 0;		//Determines if triangles are up or down//
		int counter = 0;	//Counter for letter printing//
		char letter = (char)(counter + 'A');		//The letter to print//
		boolean contin;						//boolean that is used in the loops to determine if it should continue to the next part in the program//
		
		//Creating scanner object//
		Scanner input = new Scanner(System.in);
		
		
		//This loop will keep the program running as long as the user don't input the exit number -1//
		while(length != -1)
		{
			contin = false;		//setting it to false for the begining of the program//
			while(!contin)
			{
				//Asking for triangle length and lets user input desired lenght//
				System.out.print("Ange längden på de 2 lika länga sidorna (avsluta med -1): ");
				length = input.nextInt();
				
				if(((length > 26) || (length == 0) || (length < 0)) && (length != -1))
				{
					System.out.println("Ange ett tal som är inom området 0 < längds < 27");
				}
				else
				{
					contin = true;
				}
			}
			contin = false;		//resetting the continue variable for use in the next loop for the upOrDown check//
			//Checks if inputed number is the exit number//
			if(length != -1)
			{
				while(!contin)
				{
					//Asking for triangle orientation and lets user input//
					System.out.print("Ska den räta vinkeln vara nedåt (0) eller uppåt (1): ");
					upOrDown = input.nextInt();
					
					if((upOrDown != 0) && (upOrDown != 1))
					{
						System.out.println("Ange ett tal som är antligen 1 eller 0");
					}
					else
					{
						contin = true;
					}
				}
				
				//using a switch to print triangle based on input//
				switch (upOrDown) 
				{
				case 0:
				{
					for(int i = 0; i < length; i++)	//Loop that keeps track of number of lines to print//
					{
						for(int j = 0; j <= i;  j++)	//Loop for printing each line//
						{
							letter = (char)(counter + 'A');
							System.out.print(letter);
							counter++;
						}
						System.out.println();
						counter = 0;
					}
					break;
				}		//End of case 0//
					
				case 1:
				{
					for(int i = length; i >= 0; i--)	//Loop that keeps track of number of lines to print//
					{
						for(int j = 1; j <= i;  j++)	//Loop for printing each line//
						{
							letter = (char)(counter + 'A');
							System.out.print(letter);
							counter++;
						}
						System.out.println();
						counter = 0;
					}
				}		//End of Case 1//

				default:
					break;
				}		//End of switch//
				
			}		//End of If statement if(length != -1)//
		}		//End of mainloop//
		//Closing the Scanner//
		input.close();
	}	//End of main function//
}
