/**
 * This progam will take the users input of how many random numbers he/she wants to generate, then the program will present these numbers, 
 * first in the order they are generated and then in a sorted order there all even numbers comes first then the uneven numbers.
 * Lastly it will present how many numbers that was even and uneven
 *
 * @author Nicklas König, nickni-6
 *
 */

import java.util.Scanner;

public class Uppgift4 
{
	public static void main(String[] args)
	{
		int numberOfRandomizedNumbers;
		int[] mainarray;					//Mainarray to hold the ranomized numbers//
		int[] sortedarray;					//array that holds the sorted numebrs//
		int evenNumbers = 0;					//Counter for even numbers randomized//
		int unevenNumbers = 0;					//Counter for uneven numbers ranomized//
		Double temp;						//Temp and temp2 is used to temporarely hold values before they are inputed into the array//
		int temp2;
		Scanner input = new Scanner(System.in);
		
		System.out.print("Hur många slumptal i intervallet 0 - 999 önskas? ");		//Asks the user to input the desired amount of numbers to randomize//
		numberOfRandomizedNumbers = input.nextInt();
		
		mainarray = new int[numberOfRandomizedNumbers];
		sortedarray = new int[numberOfRandomizedNumbers];
		
		System.out.println("Här är de slumpade talen:");
		for(int i = 0; i < numberOfRandomizedNumbers; i++)		//Loop for randomizeding, presenting the numbers//
		{
			temp = new Double(Math.random() * 1000);
			temp2 = temp.intValue();
			mainarray[i] = temp2;
			System.out.print(mainarray[i] + "\t");
		}
		
		int j = 0;				//int to keep track of the position in the sorted array//
		//sorting the numbers//
		for(int i = 0; i< numberOfRandomizedNumbers; i++)
		{
			if(mainarray[i] %2 == 0)
			{
				sortedarray[j] = mainarray[i];
				mainarray[i] = 0;							//Setting the position to 0, for easier take out the uneven numbers for the end of the array//
				evenNumbers++;
				j++;
			}
		}
		
		//Putting the uneven numbers into the array//
		for(int i = 0; i < numberOfRandomizedNumbers; i++)
		{
			if(mainarray[i] !=0)
			{
				sortedarray[j] = mainarray[i];
				unevenNumbers++;
				j++;
			}
		}
		
		System.out.println("\nHär är de slumpade talen ordnade:");
		for(int i = 0; i < evenNumbers; i++)
		{
			System.out.print(sortedarray[i] + "\t");
		}
		
		System.out.print(" - \t");
		
		for(int i = 0; i < unevenNumbers; i++)
		{
			System.out.print(sortedarray[i + evenNumbers] + "\t");
		}
		
		System.out.println("\nAv ovanstående " + numberOfRandomizedNumbers + " tal var " + evenNumbers + " jämna och " + unevenNumbers + " udda.");
		
		input.close();		//Closing the scanner//
	}
}
