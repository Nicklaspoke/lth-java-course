/**
 * This program will take inputs of radius and height to calculate area, mantelarea and volume of a cone. It will also take values of nominators and denominators
 *to calculate fractions.
 * @author Nicklas König, nickni-6
 *
 */


import java.math.*;
import java.util.Scanner;


public class Uppgift5 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int nominator = 0;
		int denominator = 0;
		int radius = 0;
		int height = 0;
		boolean quit = false;	//bool that desermens if the program should continue to the next part of the program or not aka if the user has inputed 'q' or not//
		
		//this set of booleans checks if a value has been assigned to to both variables before doing any calculations
		boolean hasRadius = false;
		boolean hasRadiusAndHeight = false;
		boolean hasNominatorAssigned = false;
		boolean nominatorAnddenominaterAssigned = false;
		
		System.out.println("----------------------------------");
		System.out.println("# Test av area och volymmetoderna");
		System.out.println("----------------------------------");
		System.out.print("> ");
		
		//asuming the user dosen't input a uneven number of intergers//
		while(!quit)
		{
			if(input.hasNextInt())
			{
				if(!hasRadius)
				{
					radius = input.nextInt();
					radius = Math.abs(radius);
					hasRadius = true;
				}
				else
				{
					height = input.nextInt();
					height = Math.abs(height);
					hasRadiusAndHeight = true;
				}
				
				if(hasRadiusAndHeight)
				{
					System.out.println("r = " + radius + " h = " + height);
					System.out.println("Basytans area: " + area(radius));
					System.out.println("Mantelytans area: " + area(radius, height));
					System.out.println("Volym: " + volume(radius, height));
					System.out.println();
					
					hasRadius = false;
					hasRadiusAndHeight = false;
				}
			}
			
			if(!input.hasNextInt())
			{
				String s;
				s = input.next();
				if(s.equals("q"))
				{
					quit = true;
				}
			}
			
		}
		
		quit = false;		//resetting the variable for exiting the loop//
		
		System.out.println("----------------------------------");
		System.out.println("# Test av bråktalsmetoderna");
		System.out.println("----------------------------------");
		System.out.print("> ");
		while(!quit)
		{
			if(input.hasNextInt())
			{
				if(!hasNominatorAssigned)		//checks if nominator has been assigned a value//
				{
					nominator = input.nextInt();
					hasNominatorAssigned = true;
				}
				else
				{
					denominator = input.nextInt();
					nominatorAnddenominaterAssigned = true;
				}
				
				if(nominatorAnddenominaterAssigned)
				{
					System.out.print(nominator + "/" + denominator + " = ");
					printFraction(fraction(nominator, denominator));
					hasNominatorAssigned = false;
					nominatorAnddenominaterAssigned = false;
				}
			}
			
			if(!input.hasNextInt())
			{
				String s;
				s = input.next();
				if(s.equals("q"))
				{
					quit = true;
				}
			}
		}
		
		input.close();
	}
	
	
	//This function will take the inputed radius of the circle and return the area//
	public static float area(int radius)
	{
		float area;
		
		area = (float) (Math.pow(radius, 2) * Math.PI);
		
		return area;
	}
	
	//This function will calculate the mantelarea of a cone
	public static float area(int radius, int height)
	{
		float mantelArea;
		float s = pythagoras(radius, height);
		
		mantelArea = (float) (Math.PI * radius * s);
		
		
		return mantelArea;
	}
	
	public static float pythagoras(int a, int b)
	{
		float c;
		int temp;		//This int will hold the value of a^2 + b^2
		
		temp = (int) (Math.pow(a, 2) + Math.pow(b, 2));
		
		c = (float) Math.sqrt(temp);
		
		
		return c;
	}
	
	
	//This function will calculate the valoume of a cone
	public static float volume(int radius, int height)
	{
		float volume;
		
		volume = (float) ((Math.PI * Math.pow(radius, 2) * height) / 3);
		
		return volume;
	}
	
	public static int[] fraction(int nominator, int denominator)
	{
		int[] mainarray = new int[3];
		int remainder;
		int gcd;
		
		if(denominator == 0)
		{
			return null;
		}
		
		if(nominator == 0)
		{
			mainarray[0] = 0;
			mainarray[1] = 0;
			mainarray[2] = 0;
			
			return mainarray;
		}
		
		mainarray[0] = nominator / denominator;
		remainder = nominator % denominator;
		gcd = sgd(remainder, denominator);
		mainarray[1] = remainder / gcd;
		mainarray[2] = denominator / gcd;
		
		return mainarray;
	}
	
	//This function will use Euklides algotithm to find the greatest common divider between two numbers//
	public static int sgd(int a, int b)
	{
		int c;
		
		if (b > a)
		{
			int teampholder = a;
			a = b;
			b = teampholder;
		}
		
		while(b != 0)
		{
			c = a % b;
			a = b;
			b = c;
		}
		return a;
	}
	
	public static void printFraction(int[] parts)
	{
		
		if(parts == null)
		{
			System.out.println("Error");
			return;
		}
		
		if(parts[0] != 0 && parts[1] != 0)
		{
			System.out.println(parts[0] + " " + parts[1] + "/" + parts[2]);
			return;
		}
		
		if(parts[0] != 0 && parts[1] == 0)
		{
			System.out.println(parts[0]);
			return;
		}
		
		if(parts[0] == 0 && parts[1] != 0)
		{
			System.out.println(parts[1] + "/" + parts[2]);
			return;
		}
		
		if(parts[2] == 0)
		{
			System.out.println("0");
			return;
		}
		
	}
	
	
}