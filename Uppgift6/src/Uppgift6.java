/**
 * This program will simulate a simple ATM, you will be able to choose form a menu, to deposit, withdraw and view your balance
 * @author Nicklas König, nickni-6
 *
 */

import java.util.Scanner;

public class Uppgift6 
{
	public static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		int choice = 0;		//variable that will catch the return value from the menu function//
		int balance = 1000;		//The assignment didn't specifiy any amount for the balance so I made it to 1000//
		int[] transactions = new int[10]; 		//Change the number inside the square brakets to allow the program to store more than 10 transactions//
		int temp;		//variable to temporarily store the value of the input, so it can be sent in to the function//
		while(choice !=4)		//while loop to keep the program running until the user inputs the exit number (4)//
		{
			choice = menu();
			switch(choice)
			{
			case 1:
			{
				String msg = "Hur mycket ska sättas in på kontot?: ";
				temp = input(msg);
				makeTransaction(transactions, temp);
				balance += temp;
				System.out.println(findNr(transactions));
				break;
			}	//End of case 1
			
			case 2:
			{
				String msg = "Hur mycket ska tas ut från kontot?: ";
				temp = input(msg);
				if(balance - temp < 0)
				{
					System.out.println("Inte tillräckligt med pengar på kontot för att göra dit uttag på: " + temp + ":-");
				}
				else
				{
					temp *= -1;
					makeTransaction(transactions, temp);
					balance += temp;
				}
				
				break;
			}	//End of case 2
			
			case 3:
			{
				showTransactions(transactions, balance);
				break;
			}	//End of case 3
			
			default:
			{
				break;
			}
			}	//End of switch
		}	//End of loop
	
	}	//End of main
	public static int menu()
	{
		int choice;
		while(true)
		{
			System.out.println("1. Insättning");
			System.out.println("2. Uttag");
			System.out.println("3. Saldobesked");
			System.out.println("4. Avsluta");
			System.out.print("Ditt val: ");
			if(input.hasNext())
			{
				if(input.hasNextInt())
				{
					choice = input.nextInt();
					if((choice > 4) || (choice < 1))
					{
						System.out.println("Var god och ange et giltligt värde för inmatning");
					}	//end of if statement
					else
					{
						return choice;
					}	//End of else statement
				}
				else
				{
					System.out.println("Var god och ange et giltligt värde för inmatning");
				}
				
			}
			input.nextLine();
			choice = 0;		//resetting choice so it doesen't cause an infinite loop
		}		//end of while loop
		
	}
	
	public static int input(String msg)
	{
		int choice;
		while(true)
		{
			choice = 0;
			System.out.println(msg);
			if(input.hasNext())
			{
				if(input.hasNextInt())
				{
					choice = input.nextInt();
					if(choice < 1)
					{
						System.out.println("Var god och ange et giltligt värde för inmatning");
					}	//End of third if statement
					else
					{
						return choice;
					}
				}	//End of second if statement
				else
				{
					System.out.println("Var god och ange et giltligt värde för inmatning");
				}
			}	//End of first if statement
			input.nextLine();
		}	//End of while loop
		
		
		
	}	//end of function input
	
	public static void showTransactions(int[] trans, int balance)
	{
		System.out.println("Ditt saldo på kontot är " + balance + ":-");
		System.out.println("Dina senaste transektioner: ");
		for(int i = 0; i < trans.length; i++)
		{
			if(trans[i] != 0)		//If statement so that the program dosen't print out any zeros, aka no transaction stored in the array on that place//
			{
				System.out.println(trans[i]);
			}
		}	//End of for loop
	}	//end of function showTransactions
	
	public static void makeTransaction(int[] trans, int amount)
	{
		int temp = findNr(trans);	//Saves the index number for the next available slot in the array to insert the transaction//
		if(temp == -1)
		{
			moveTrans(trans);
			trans[trans.length - 1] = amount;
		}
		else
		{
			trans[temp] = amount;
		}
	}	//end of function makeTransaction
	
	private static int findNr(int[] trans)
	{
		for(int i = 0; i < trans.length; i++)
		{
			if(trans[i] == 0)
			{
				return i;
			}
		}
		return -1;
	}	//end of function findNr
	
	private static void moveTrans(int[] trans)
	{
		for(int i = 1; i < trans.length; i++)
		{
			trans[i-1] = trans[i];
		}
	}	//end of function moveTrans
}	//end of class