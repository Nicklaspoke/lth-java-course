/**
 * This program will train the user in English words from the popular game minecraft, every time the user takes the test, 10 words will
 * be randomized from a pool of 20 words.
 * @author Nicklas König, nickni-6
 *
 */


import java.util.Scanner;

public class Tentamen_VT17_nickni6
{
	
	public static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		String[] swedish_words = new String[20];		//creating arrays for storing the swedish and English words//
		String[] english_words = new String[20];
		int[] wordsAnsweredRight = new int[10];			//Creating two arrays to store the index numbers for words answered, for presentation at the end//
		int[] wordsAnsweredWrong = new int[10];
		int[] wordsToAsk = new int[10];					//Array that will be randomized for each test the user wants to take//
		wordsToAsk[9] = 100;
		int numberOfRightAnswers = 0;
		int numberOfWrongAnswers = 0;
		String userInput;
		
		Boolean quit = false;		//boolean that keeps the test running until the user wants to exit//
		
		assign_words(swedish_words, english_words);		//Sending arrays into function to assign the words//
		
		while(!quit)		//Main loop for the program//
		{
			randomizeQuestions(wordsToAsk);				//Randomizing 10 new questions every time user wants to take the test//
			
			for(int i = 0; i < 10; i++)
			{
				userInput = PresentQuestion(i, wordsToAsk[i], swedish_words);
				
				if(checkIfWordIsRight(english_words, wordsToAsk[i], userInput))
				{
					System.out.println("Rätt!");
					wordsAnsweredRight[numberOfRightAnswers] = wordsToAsk[i];
					numberOfRightAnswers++;
				}
				else
				{
					System.out.println("Fel, rätt svar är: " + english_words[wordsToAsk[i]]);
					wordsAnsweredWrong[numberOfWrongAnswers] = wordsToAsk[i];
					numberOfWrongAnswers++;
				}
				
			}
			presentResult(numberOfRightAnswers, numberOfWrongAnswers, wordsAnsweredRight, wordsAnsweredWrong, english_words);
			
			while(true)		//Don't know if error check is necessary here, the exam didn't specify. Assuming error check for input is needed//
			{
				System.out.print("Vill du göra ett nytt test? [Y/N]: ");
				userInput = input.nextLine();
				
				if((userInput.equals("Y")) || (userInput.equals("y")))
				{
					quit = false;
					newTest(wordsToAsk);			//Function used to reset the array for the next test//
					numberOfRightAnswers = 0;
					numberOfWrongAnswers = 0;
					break;
				}
				else if ((userInput.equals("N")) || (userInput.equals("n"))) 
				{
					quit = true;
					break;
				}
				else 
				{
					System.out.println("Vänligen ange ett giltligt inmatningsvärde");
				}
				
			}		//End of while loop for quit checking//
		}		//End of main loop in main
	}		//End of main
	
	private static void newTest(int[] wordsToAsk) 
	{
		for(int i = 0; i < 9; i++)
		{
			wordsToAsk[i] = 0;
		}
		wordsToAsk[9] = 100;
	}

	public static String PresentQuestion(int i , int index, String[] swedish_words)
	{
		System.out.print((i+1) + ". " + swedish_words[index] + " på engelska: ");
		String buffer;
		buffer = input.nextLine();
		return buffer;
	}
	
	public static Boolean checkIfWordIsRight(String[] english_words,int index, String userInput)
	{
		if(userInput.equals(english_words[index]))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static void presentResult(int numberOfRightAnswers, int numberOfWrongAnswers, int[] wordsAnsweredRight, int[] wordsAnsweredWrong, String[] english_words)
	{
		System.out.println("Du hade " + numberOfRightAnswers + " rätt");
		System.out.print("Följande ord hade du rätt på: ");
		for(int i = 0; i < numberOfRightAnswers; i++)
		{
			System.out.print(english_words[wordsAnsweredRight[i]]);
			if (i != numberOfRightAnswers - 1) 
			{
				System.out.print(", ");
			}
		}
		
		System.out.println();
		
		System.out.print("Följande ord hade du fel på: ");
		for(int i = 0; i < numberOfWrongAnswers; i++)
		{
			System.out.print(english_words[wordsAnsweredWrong[i]]);
			if(i != numberOfWrongAnswers - 1)
			{
				System.out.print(", ");
			}
		}
		System.out.println();
	}
	
	
	//This function uses the principle of a Linear congruential generator for generating the random values//
	//The function uses the same a and c as the java.util.random functions//
	//Source: https://en.wikipedia.org/wiki/Linear_congruential_generator//
	public static void randomizeQuestions(int[] wordsToAsk)
	{
		final int a = 2147483647;		//The max value of an int//
		final int c = 11;
		long seed = System.currentTimeMillis();
		int buffer;
		Boolean flag = false;
		int i = 0;
		
		while(wordsToAsk[9] == 100)
		{
			buffer = (int) ((a * seed + c) % 20);
			seed = seed-System.nanoTime();
			for(int j = 0; j < 10; j++)
			{
				if((buffer == wordsToAsk[j]) || ((buffer * -1) == wordsToAsk[j]))
				{
					flag = true;
				}
			}
			if(!flag)
			{
				if(buffer < 0)		//Checking in case the generated number is negative//
				{
					buffer *= -1;
				}
				wordsToAsk[i] = buffer;
				i++;
			}
			flag = false;
			seed = System.currentTimeMillis();		//Using system time as a seed//
			
		}
	}
	public static void assign_words(String[] swedish_words, String[] english_words)		//Hard coding the words for the arrays because a reading from a file can't be used in this assignment//
	{
		swedish_words[0] = "banderoll";
		english_words[0] = "banner";
		
		swedish_words[1] = "krut";
		english_words[1] = "gunpowder";
		
		swedish_words[2] = "lera";
		english_words[2] = "clay";
		
		swedish_words[3] = "kittel";
		english_words[3] = "cauldron";
		
		swedish_words[4] = "staket";
		english_words[4] = "fence";
		
		swedish_words[5] = "tulpan";
		english_words[5] = "tulip";
		
		swedish_words[6] = "magisk dryck";
		english_words[6] = "potion";
		
		swedish_words[7] = "sockerrör";
		english_words[7] = "sugar canes";
		
		swedish_words[8] = "städ";
		english_words[8] = "anvil";
		
		swedish_words[9] = "klocka";
		english_words[9] = "clock";
		
		swedish_words[10] = "fackla";
		english_words[10] = "torch";
		
		swedish_words[11] = "grus";
		english_words[11] = "gravel";
		
		swedish_words[12] = "trä";
		english_words[12] = "wood";
		
		swedish_words[13] = "stege";
		english_words[13] = "ladder";
		
		swedish_words[14] = "snöboll";
		english_words[14] = "snowball";
		
		swedish_words[15] = "läder";
		english_words[15] = "leather";
		
		swedish_words[16] = "kista";
		english_words[16] = "chest";
		
		swedish_words[17] = "kaktus";
		english_words[17] = "cactus";
		
		swedish_words[18] = "flinta";
		english_words[18] = "flint";
		
		swedish_words[19] = "järn";
		english_words[19] = "iron";
	}
}
