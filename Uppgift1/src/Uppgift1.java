/**
 * The function of this class is to print a chart with the three provided elements and to calculate their mass number with the math.round function
 * @author Nicklas König, nickni-6
 *
 */

public class Uppgift1 
{
	public static void main(String[] args)
	{
		//Using the function Math.round, to round out the calculation for the Mass number for each element in the chart that is going to be printed//
		long ag_Mass_Number = Math.round(107.8682-47);
		long au_Mass_Number = Math.round(196.96654-79);
		long c_Mass_Number = Math.round(12.01-6);
		
		//Printing the chart for the three elements provided in the task//
		System.out.println("Tecken\t Grundämne\t Atomnummer\t Atommassa\t Masstal");
		System.out.println("====================================================================");
		System.out.println("Ag\t Silver\t\t 47\t\t 107.8682\t " + ag_Mass_Number);
		System.out.println("Au\t Guld\t\t 79\t\t 196.96654\t " + au_Mass_Number);
		System.out.println("C\t Kol\t\t 6\t\t 12.01\t\t " + c_Mass_Number);
	}
}
